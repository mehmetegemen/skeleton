export default {
    'MONGO_URL': process.env.MONGO_URL,
    'PORT': process.env.PORT || 3002,
    'USERS_SERVICE_URL': process.env.USERS_SERVICE_URL,
    'SECRET': process.env.SECRET
}