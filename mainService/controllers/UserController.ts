import * as usersRest from '../models/users-rest'
import debug from 'debug'
import { Request, Response } from 'express';

const error = debug('main:error');

export default {
    async register (req: Request, res: Response) {
        try {
            let user: any = await usersRest.find(req.body);
            // Check if user exists
            if (typeof user.success === 'undefined') {
                // User exists
                // Exit with failure
                return res.send(
                    {
                        success: false,
                        message: 'User already exists.'
                    }
                )
            }

            // User does not exist
            // Begin to create
            user = await usersRest.create(req.body);
            res.send(user);
        } catch (err) {
            error(`Register controller error: ${err.stack}`);
            res.status(500).send(
                {
                    success: false,
                    message: 'Register error.'
                }
            );
        }
    }
}