import { Document, Schema, Model, model } from 'mongoose'
import { IUser } from '../interfaces/User'
import crypto from 'crypto'

export interface IUserModel extends IUser, Document {};

export let UserSchema: Schema = new Schema({
    birthDate: Date,
    firstName: String,
    familyName: String,
    password: String,
    email: String,
    provider: String,
    accessToken: String
});

export function passwordHash(password: string): string {
    const hash = crypto.createHash("sha512");
    hash.update(password);
    return hash.digest('hex');
}

export const User: Model<IUserModel> = model<IUserModel>("User", UserSchema);